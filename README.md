# ALL IN ONE

## Current Version: RELEASED 1.0.1

### Change Log: Critical bugs fixed, QuadReg added

A TI-Basic program that do math for you

––––––––––––––––––––––––––––Improvements––––––––––––––––––––––––––––

• Better, but smaller!

Reduced **56%** of the size From ***33943*** to ***14750*** (1888 lines in calculator) and make the program execution faster with smaller size

• More compatibility!

Now support TI 84 Plus C Silver Edition and TI 84 Plus CE!

• More function!

Quadratic Equation Solver added;

Factoring Polynomials added;

QuadReg added

Details:

Deleted a ***Ton*** of Unnecessary code to make it small enough to fit into TI 84 Plus C Silver Edition Calculators

Capitalize all lower case letters in strings to make it need less space (each lower-case letter is 2 bytes, while upper-case is only 1 byte)

––––––––––––––––––––––––––––Installation––––––––––––––––––––––––––––

Visit: https://gitlab.com/Waaangyi/all-in-one-ce/wikis/Installation

––––––––––––––––––––––––––––Contact and Misc.––––––––––––––––––––––––––––

Optimizations and Ideas are welcome!

If you found problems or possible optimizations post on the following link:

https://www.cemetech.net/forum/viewtopic.php?p=272643#272643




